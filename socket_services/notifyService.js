const io = require('socket.io-client');
require('dotenv').config();

const socket = io(process.env.IP_SERVICE_DTMS, {
    transportOptions: {
        polling: {
            extraHeaders: {
                "X-APIKEY": 'am1zLXNlcnZpY2UtY29ubmVjdGVk'
            }
        }
    },
    reconnection: true
})




socket.on('error-message', data => {
    console.log(data)
})


socket.on('connect', data => {
    if (socket.connected) {
        console.log("Socket DTMS started !")
    } else {
        console.log("Socket DTMS disconnected")
    }
})

socket.on('disconnect', data => {
    console.log("Socket DTMS disconnected")
})

module.exports = {
    socket
}