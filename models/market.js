const mongoose = require('mongoose')
var dbConfig = require('../config/db')

var ItemSchema = mongoose.Schema({
    name: String,
    cost: Number,
    img: String,
    quantities: Number,
    createdTime: Date,
    discount: Number,
    creatorID: String,
    code: String,
    requirementLevel: Number,
    isReward: Boolean
})

var marketModel = dbConfig.initDB.model("Market", ItemSchema)

exports.get = (query, limit, offset, cb) => {
    marketModel.find(query, (err, item) => {
        if (err) return cb(err)
        return cb(err, item)
    }).limit(limit).skip(offset)
}

exports.all = (query, cb) => {
    marketModel.find(query, (err, item) => {
        if (err) return cb(err)
        return cb(err, item)
    })
}

exports.update = (querySearch, queryUpdate, cb) => {
    marketModel.update(querySearch, queryUpdate, (err, item) => {
        if (err) return cb(err)
        return cb(err, item)
    })
}