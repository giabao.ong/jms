var bcrypt = require('bcrypt');
var moment = require('moment')
var db = require('../config/db')
var mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    username: String,
    password: String,
    name: String,
    age: Number,
    avatar: String,
    gender: String,
    email: String,
    phone: String,
    address: String,
    position: String,
    role: String,
    bureau: String,
    level: Number,
    experience: Number,
    coin: Number,
    happy: Number,
    skill: [],
    power: Number,
    rank: Number,
    mentor: Array,
    trainee: Array,
    createdTime: Date
})

var UserModel = db.initDB.model("User", userSchema);

exports.getFromId = (id, cb) => {
    UserModel.findOne({ _id: id }, function(err, data) {
        if (err) return cb(err);
        return cb(null, data);
    })
}

exports.updateUser = (query, data, cb) => {
    UserModel.updateOne(query, data, (err, user) => {
        if (err) return cb(err)
        UserModel.find(query, (err, u) => {
            if (err) return cb(err)
            return cb(null, u)
        })
    })
}

exports.getAny = (query, cb) => {
    UserModel.find(query, (err, users) => {
        return cb(err, users)
    })
}