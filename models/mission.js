var dbConfig = require('../config/db')
var mongoose = require('mongoose')

var missionSchema = new mongoose.Schema({
    title: String,
    description: String,
    img: String,
    deadline: Date,
    createdTime: Date,
    creator: { _id: String, avatar: String, name: String },
    coin: Number,
    experience: Number,
    type: String,
    target: Array,
    outOfDate: Boolean
})

var missionLogsSchema = new mongoose.Schema({
    img: String,
    deadline: Date,
    type: String,
    title: String,
    description: String,
    missionID: String,
    userID: String,
    status: String,
    coin: Number,
    experience: Number,
    completedTime: Date
})

var missionModel = dbConfig.initDB.model("Mission", missionSchema)
var missionLogsModel = dbConfig.initDB.model("Mission_Logs", missionLogsSchema)

exports.create = (data, cb) => {
    missionModel.create(data, (err, mission) => {
        if (err) return cb(err)
        return cb(err, mission)
    })
}

exports.get = (query, limit, offset, cb) => {
    missionModel.find(query, (err, mission) => {
        if (err) return cb(err)
        return cb(err, mission)
    }).limit(limit).skip(offset).sort({ createdTime: -1 })
}

exports.done = (data, cb) => {
    missionLogsModel.create(data, (err, mission) => {
        if (err) return cb(err)
        return cb(err, mission)
    })
}

exports.getLogs = (data, limit, offset, cb) => {
    missionLogsModel.find(data, (err, mission) => {
        if (err) return cb(err)
        return cb(err, mission)
    }).limit(limit).skip(offset)
}