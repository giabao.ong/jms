var express = require('express');
var cors = require('cors')
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require("compression")
var dbConfig = require('./config/db')
    // var jobWorker = require('./jobs/system/retry')
var axios = require('axios')
require('dotenv').config();
var app = express();

dbConfig.initDB
dbConfig.initDBLog


const version = "v1"
const appName = "jms"
const url = "/" + appName + "/" + version

app.set('url', url)

process.on('uncaughtException', (err, origin) => {
    axios.post(`http://api.telegram.org/bot1319027140:AAEC7QwlZRh_Vbygv352GtLwmc1gDa5a2a0/sendMessage?chat_id=-1001200490767&text= ! JMS crashed ${err.stack}`)
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// uncomment after placing your favicon in /public
app.use(compression());
app.set("env", process.env.NODE_ENV || "development");
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// các cài đặt cần thiết cho passport
app.use(cors())
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Credentials', 'true')
    next()
})

// test.verify({ function: "done", _id: "123" })
// var timer
// jobWorker.retry_job(600, 0, (timer, err) => {
//     console.log("ping")
//     if (err.isNoData == true) {
//         timer._repeat = 60000 * 5
//     }
// })


app.get(url, (req, res) => {
    res.send("JMS started").status(200);
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500).send({ message: err.message });
});


module.exports = app;