var miniLevel = require('./missions/min-level')

var mini_level = (mission, next) => {
    miniLevel.mini_level(mission, err => {
        return next(err)
    })
}

module.exports = {
    mini_level
}