var jobModel = require('../../models/jobs')
var missionController = require('../../controllers/missionController')
var taskController = require('../../controllers/taskController')
var moment = require('moment')

exports.retry_job = (time, pod, cb) => {
    timeInter = setInterval(() => {
        jobModel.getAnyJobs({ status: "FAILED", updatedTime: { $lte: moment().add(-3, "minute").format() } }, 1, 0, (err, jobs) => {
            if (err) {
                return err(err)
            } else if (jobs.length > 0) {
                var job = jobs[0]
                switch (job.action) {
                    case "VERIFY_MISSION":
                        // don't need

                        // missionController.verify(job.data, err => {
                        //     if (err == null) {
                        //         missionController.done(job.data, start, job)
                        //     } else {
                        //         var retryTime = parseInt(job.retryTime) + 1
                        //         jobModel.updateJob({ _id: job._id }, { listErrors: err, status: "FAILED", retryTime: retryTime }, (err, j) => {})
                        //     }
                        // })
                        break;
                    case "PROCESS_AFTER_TASK_DONE":
                        taskController.consumed(job, err => {
                            if (err == null) {
                                job.status = "COMPLETED"
                                jobModel.consumedJob(job._id, job, start, (err, jobConsumed) => {})
                            } else {
                                job.status = "FAILED"
                                var retryTime = parseInt(job.retryTime) + 1
                                jobModel.updateJob({ _id: job._id }, { listErrors: err, retryTime: retryTime }, (err, j) => {})
                            }
                        })
                        break;
                }
            } else {
                cb(timeInter, { isNoData: true })
            }
        })
    }, time)

}