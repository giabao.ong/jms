var moment = require('moment')
var missionDB = require('../../models/mission')
var userDB = require('../../models/user')
var postDB = require('../../models/post')

var mini_level = async(mission, next) => {
    await userDB.getFromId(mission.currentUser, (err, usr) => {
        if (usr.level == 2) {
            var newExp = mission.experience + usr.experience
            var newCoin = mission.coin + usr.coin
            userDB.updateUser({ _id: mission.currentUser }, { experience: newExp, coin: newCoin }, (err, newUser) => {
                if (err)
                    return next({ status: 400, message: "Cannot update user" })

                if (newUser) {
                    return next()
                } else {
                    return next({ status: 400, message: "Cannot update user" })
                }
            })
        } else {
            return next({ status: 400, message: "You cannot complete this mission." })
        }
    })
}

var first_post = (mission, next) => {
    postDB.getPosts({ "creator._id": mission.currentUser }, 1, 0, (err, post) => {
        if (post.length == 1) {
            userDB.getFromId(mission.currentUser, (err, usr) => {
                var newExp = mission.experience + usr.experience
                var newCoin = mission.coin + usr.coin
                userDB.updateUser({ _id: mission.currentUser }, { experience: newExp, coin: newCoin }, (err, newUser) => {
                    if (err)
                        return next({ status: 400, message: "Cannot update user" })

                    if (newUser) {
                        return next()
                    } else {
                        return next({ status: 400, message: "Cannot update user" })
                    }
                })
            })
        } else {
            return next({ status: 400, message: "You cannot complete this mission." })
        }
    })

}

module.exports = {
    mini_level,
    first_post
}