var db = require('mongoose');
var db_logs = require('mongoose');
require('dotenv').config();


// DB Connection
var initDB = db.createConnection(process.env.DB_CONNECT_PRD, { useNewUrlParser: true, useUnifiedTopology: true })
initDB.then(() => console.log("DB Connected")).catch(err => console.log(err))

var initDBLog = db_logs.createConnection(process.env.DB_CONNECT_PRD_LOGS, { useNewUrlParser: true, useUnifiedTopology: true })
initDBLog.then(() => console.log("DB Logs connected")).catch(err => console.log(err))


module.exports = {
    initDBLog,
    initDB
}