var missionDB = require('../models/mission')
var notify = require('../socket_services/notifyService')
var userDB = require('../models/user')
var jobModel = require('../models/jobs')
var moment = require('moment')
    /**TODO refactor */
var jobs = require('../jobs/missions/firstMission')
var levelController = require('./levelController')

exports.verify = async(data, cb) => {
    var mission = data
    if (mission.length >= 1)
        mission = data[0]

    if (mission._id == undefined || mission._id == "") {
        return cb({ message: "Mission ID is null", status: 500 })
    }

    var fn = jobs[mission.function]
    if (typeof fn === "function") {
        await fn.apply(null, [mission, cb])
    } else {
        return cb({ message: "Function not found", status: 500 })
    }
}

exports.done = (req, start, job) => {
    var mission = req
    if (mission.length >= 1)
        mission = req[0]
    job.status = "COMPLETED"
    jobModel.consumedJob(job._id, job, start, (err, job) => {})
    var log = {
        img: mission.img,
        type: mission.type,
        deadline: mission.deadline,
        title: mission.title,
        description: mission.description,
        missionID: mission._id,
        userID: mission.currentUser,
        status: "DONE",
        coin: mission.coin,
        experience: mission.experience,
        completedTime: moment().format()
    }
    missionDB.done(log, (err, l) => {
        if (l) {
            //push to dtms
            notify.socket.emit('mission', { target: req[0].currentUser, message: "Mission completed !", status: 200, missionID: req[0]._id })
            levelController.consumed(req[0].currentUser, (err) => {})
        } else {
            //push to dtms
            jobModel.updateJob({ _id: job._id }, { listErrors: err || [{ message: "Cannot verify this mission" }], status: "FAILED" }, (err, j) => {
                // jobModel.consumedJob(j._id, j, start, (err, job) => {})
            })
            notify.socket.emit('mission', { target: req[0].currentUser, message: "Mission incompleted !", status: 400, missionID: req[0]._id })
        }
    })
}

exports.fail = (job, err, start) => {
    jobModel.updateJob({ _id: job._id }, { listErrors: err || [{ message: "Cannot verify this mission" }], status: "FAILED" }, (err, j) => {
            // jobModel.consumedJob(j._id, j, start, (err, job) => {})
        })
        //push to dtms
    notify.socket.emit('mission', { target: job.currentUser, message: err.message, status: 400, missionID: job._id })
}