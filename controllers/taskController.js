var userDB = require("../models/user")
var levelController = require('./levelController')
var moment = require('moment')
exports.consumed = (req, next) => {
    if (req.data == null) {
        return next({ message: "Data null", status: 500 })
    }

    var task = req.data
    var listUser = task.takenBy
    if (listUser.length == 0)
        return next({ message: "Members empty", status: 500 })

    var listUserId = []
    for (const u of listUser) {
        listUserId.push(u._id);
    }

    var query = {
        _id: { $in: listUserId }
    }


    userDB.getAny(query, (err, usr) => {
        if (usr.length > 0) {
            var done = 0
            usr.forEach(u => {
                if (req.isBeforeDeadline) {
                    var dateDone = moment(task.completedTime)
                    var deadline = moment(task.deadline)
                    var diff = Math.abs(dateDone.diff(deadline, "days")) + 1
                    var currentExp = parseInt(u.experience)
                    var newExp = currentExp + parseInt(1 * task.difficulty * diff)
                    var coin = parseInt(u.coin)
                    var newCoin = coin + parseInt(5 * task.difficulty * diff)
                    userDB.updateUser({ _id: u._id }, { experience: newExp, coin: newCoin }, (err, user) => {
                        if (user.length > 0) {
                            levelController.consumed(user[0]._id, () => {})
                        } else {

                        }
                    })

                } else {
                    var currentHappy = parseInt(u.happy);
                    var newHappy = currentHappy - 5;
                    userDB.updateUser({ _id: u._id }, { happy: newHappy, }, (err, user) => {})
                }

                done++;
                if (done == usr.length) {
                    next(null)
                }
            })
        }
    })

}