var io = require('socket.io')
var app = require('./app')
var missionController = require('./controllers/missionController')
var taskController = require('./controllers/taskController')
var levelController = require('./controllers/levelController')
var moment = require('moment')
var jobModel = require('./models/jobs')

const listener = (port) => io.listen(port)
const init = socket => {

    socket.of(app.get('url')).on('connection', socketIO => {

        var socketID = socketIO.id

        if (socketIO.handshake.headers["x-apikey"] === "eXVoLXNlcnZpY2UtY29ubmVjdGVk") {
            //yuh-service
            socketIO.on('/mission/done', async req => {
                var newJob = {
                    data: req[0] || {},
                    status: "PROCESSING",
                    createdTime: moment().format(),
                    keys: [req[0]._id, req[0].currentUser],
                    currentUser: req[0].currentUser,
                    action: "VERIFY_MISSION",
                    missionID: req[0]._id,
                    retryTime: 0
                }

                // Start job
                var start = moment().unix()
                    /*============================*/
                    // create job verify mission
                jobModel.create(newJob, async(err, job) => {
                        await missionController.verify(req, err => {
                            if (err == null) {
                                missionController.done(req, start, job)
                            } else {
                                missionController.fail(job, err, start)
                            }
                        })
                    })
                    /*=============================*/
            })

            //message from yuh
            socketIO.on('/task/done', req => {
                var newJob = {
                    data: req.data || {},
                    status: "PROCESSING",
                    createdTime: moment().format(),
                    keys: [req.data._id, req.data.takenBy],
                    currentUser: req.takenBy,
                    action: "PROCESS_AFTER_TASK_DONE",
                    taskID: req.data._id,
                    retryTime: 0
                }

                // Start job
                var start = moment().unix()
                    /*============================*/
                    // create job verify mission
                jobModel.create(newJob, (err, job) => {
                        taskController.consumed(req, err => {
                            if (err == null) {
                                job.status = "COMPLETED"
                                jobModel.consumedJob(job._id, job, start, (err, jobConsumed) => {})
                            } else {
                                job.status = "FAILED"
                                jobModel.updateJob({ _id: job._id }, { listErrors: [err] }, (err, j) => {})
                            }
                        })
                    })
                    /*=============================*/
            })

            socketIO.on('check-level', async user => {
                var newJob = {
                    data: user || {},
                    status: "PROCESSING",
                    createdTime: moment().format(),
                    keys: [user._id],
                    currentUser: user._id,
                    action: "CHECK_LEVEL",
                    retryTime: 0
                }

                // Start job
                var start = moment().unix()
                    /*============================*/
                    // create job check-level
                jobModel.create(newJob, async(err, job) => {
                        levelController.consumed(user._id, (err) => {
                            jobModel.consumedJob(job._id, job, start, (err, jobConsumed) => {})
                        })
                    })
                    /*=============================*/
            })
        } else {
            console.log("Wrong key")
            socketIO.emit('error-message', { status: 500, message: "CONNECTION REFUSED" })
        }
    })

    socket.on('connection', data => {
        // console.log(data);
    })

}

module.exports = {
    listener,
    init
}